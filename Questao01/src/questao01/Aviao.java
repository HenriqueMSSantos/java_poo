/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package questao01;

/**
 *
 * @author henrique
 */
public class Aviao {
    private int numero;
    private Data dataDePartida;
    protected int[] vagas;
    
    public Aviao(int numero, Data dataDePartida){
        this.numero = numero;
        this.dataDePartida = dataDePartida;
        vagas = new int[300];
    }
    
    public int getNumeroDoVoo(){
        return numero;
    }
    
    public Data getData(){
        return dataDePartida;
    }
    
    public int vagas(){
        int disponiveis = 0;
        for(int i = 0; i < 300; i++){
         if(vagas[i] == 0){
             disponiveis++;
         }
        }
        return disponiveis;
    }
    
    public int proximoLivre(){
        int count = 0;
        for(int i = 0; i < vagas.length; i++){
           if(vagas[i] == 1){
               count++;
           }
        }
        return count;
    }
    
    public boolean ocupa(int vaga){
        if(vaga >=0 && vaga < vagas.length && vagas[vaga] == 0){
            vagas[vaga] = 1;
            return true;
        }
        return false;
    }
    
    public boolean verifica(int vaga){
        if(vagas[vaga] == 1){
            return true;
        }else{
            return false;
        }
    }
    
}
